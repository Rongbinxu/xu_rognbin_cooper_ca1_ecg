"""
CA1 ECG analysis

Student name: Rongbin Xu
"""

import sys

import numpy as np

import matplotlib.pyplot as plt



# re-usable functions / classes go here

def analyse_ecg(signal_filename, annotations_filename):
    # basic formatted output in Python
    print('signal filename: %s' % signal_filename)
    print('annotation filename: %s' % annotations_filename)

    # your analysis code here

    # input signals file to the numpy
    dataSignals = np.loadtxt(signal_filename, delimiter=",", skiprows=2)
    dataSignalsTimes = tuple(x[0] for x in dataSignals)  # from signals file find out data signal time, and calculate
    timeLength = dataSignalsTimes[-1] - dataSignalsTimes[0]  # the time length, and then get the sampling rate.
    samplingRate = len(dataSignals) / timeLength

    print('Sampling Rate: %3.2f' % samplingRate + ' Hz')  # print out sampling rate

    print('Time length: %2.1f' % timeLength + ' s')  # print out time length

    print('data length: %s' % len(dataSignals) + ' samples')  # print out data length


    # Print out the maximum signal value
    elemMax = dataSignals.max(axis=0)
    if elemMax[1] > elemMax[2]:
        print('max signal value: %s' % elemMax[1] + ' mV')
    else:
        print('max signal value: %s' % elemMax[2] + ' mV')


    # Print out the minimum signal value
    elemMin = dataSignals.min(axis=0)
    if elemMin[1] < elemMin[2]:
        print('min signal value: %s' % elemMin[1] + ' mV')
    else:
        print('min signal value: %s' % elemMin[2] + ' mV')


    # get number os R wave in annotation file
    numberOfR_wave = sum(1 for line in open(annotations_filename)) - 1 #get all of lines in the annotations.txt, and -1
    print('number of R wave in annotation files: %d' % numberOfR_wave)  # because the first line of annotation.txt is title


    plt.title("ECG signals graph")
    plt.xlabel("Seconds")
    plt.ylabel("Signals")

    # add legend
    plt.legend()

    # save figure
    plt.plot(dataSignals)
    plt.grid()
    plt.show()


if __name__ == '__main__':
    analyse_ecg(sys.argv[1] + '_signals.txt', sys.argv[1]+'_annotations.txt')
    analyse_ecg("input1" + '_signals.txt', "input1" + '_annotations.txt')
