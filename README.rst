CA1
===

Modify this file to complete the following questions.
Other than filling in required information, please *DO NOT* make any changes to this file's structure or the question text (to ease grading).

Student information
-------------------

Name (as on Moodle): Rongbin Xu

Sortable name (no spaces, surname then firstname): xu_rongbin

Reflection questions (after coding)
-----------------------------------

When you have finished coding, please reflect briefly on the following three questions. Marks here are awarded for engaging with the question - they're not a trick, and there is no "right" answer.

Question 1
^^^^^^^^^^

If you had much more time to work on this problem, how would you attempt to improve your code? (Suggested length: one short paragraph)

YOUR ANSWERE HERE.

Python is a new computing language, which is I never use before. So I faced to a lots of new challenges when I tried to finish this
exercise. I spent whole week typing and fixing my code, trying to get a correct result. I researched a lot of information about
python and I even bought a python's book. In my opinions, studying a new computing language really like driving car, I just do
more and more exercise, in the end, I will be good at it. So this is the reason why I hard working on it.

Question 2
^^^^^^^^^^

What is the most important thing that you learned from this lab exercise? (Suggested length: one sentence)

YOUR ANSWERE HERE.
The most important thing that I knew what real functions does python has and how to show a heartbeat graph.

Question 3
^^^^^^^^^^

What did you like/dislike the most about this lab exercise? (Suggested length: one sentence)

YOUR ANSWER HERE.
When I printed out heartbeat graph, I feel that this function is very interest and useful.

